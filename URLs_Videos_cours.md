### Planning séances

- https://docs.google.com/spreadsheets/d/1GchlSkIG1qOl44e4MVFy7N8Rk4BBd0DfQr16pJ32sYk/edit?usp=sharing

### Cours 1 (Galaxies) :
- https://pod.univ-cotedazur.fr/media/videos/709b113f2cd8347d4a7c80f6fa8851de9cd4e63a6582a850a9b7b908cb35e981/14593/720p.mp4
- https://pod.univ-cotedazur.fr/media/videos/709b113f2cd8347d4a7c80f6fa8851de9cd4e63a6582a850a9b7b908cb35e981/14594/720p.mp4
- https://pod.univ-cotedazur.fr/media/videos/709b113f2cd8347d4a7c80f6fa8851de9cd4e63a6582a850a9b7b908cb35e981/14596/720p.mp4

### Cours 2 (Amas) :
- https://pod.univ-cotedazur.fr/media/videos/709b113f2cd8347d4a7c80f6fa8851de9cd4e63a6582a850a9b7b908cb35e981/14682/720p.mp4

### Cours 3 (Étoiles) :
- https://pod.univ-cotedazur.fr/media/videos/709b113f2cd8347d4a7c80f6fa8851de9cd4e63a6582a850a9b7b908cb35e981/14730/720p.mp4

### Cours 4 (Exoplanètes)
- https://pod.univ-cotedazur.fr/media/videos/709b113f2cd8347d4a7c80f6fa8851de9cd4e63a6582a850a9b7b908cb35e981/14870/720p.mp4
- https://pod.univ-cotedazur.fr/media/videos/709b113f2cd8347d4a7c80f6fa8851de9cd4e63a6582a850a9b7b908cb35e981/14871/720p.mp4

### Cours 5 (Petits corps du Système solaire)  - pré-cours:
- https://pod.univ-cotedazur.fr/media/videos/c15b2008a6a431b3c62a2c0b4f4a5f04548aa0f5a77088c653b52b400d36c014/14448/720p.mp4
- https://pod.univ-cotedazur.fr/media/videos/c15b2008a6a431b3c62a2c0b4f4a5f04548aa0f5a77088c653b52b400d36c014/14475/720p.mp4
- https://pod.univ-cotedazur.fr/media/videos/c15b2008a6a431b3c62a2c0b4f4a5f04548aa0f5a77088c653b52b400d36c014/14476/720p.mp4
- https://pod.univ-cotedazur.fr/media/videos/c15b2008a6a431b3c62a2c0b4f4a5f04548aa0f5a77088c653b52b400d36c014/14477/720p.mp4
- https://pod.univ-cotedazur.fr/media/videos/c15b2008a6a431b3c62a2c0b4f4a5f04548aa0f5a77088c653b52b400d36c014/14478/720p.mp4
- https://pod.univ-cotedazur.fr/media/videos/c15b2008a6a431b3c62a2c0b4f4a5f04548aa0f5a77088c653b52b400d36c014/14479/720p.mp4
- https://pod.univ-cotedazur.fr/media/videos/c15b2008a6a431b3c62a2c0b4f4a5f04548aa0f5a77088c653b52b400d36c014/14480/720p.mp4
- https://pod.univ-cotedazur.fr/media/videos/c15b2008a6a431b3c62a2c0b4f4a5f04548aa0f5a77088c653b52b400d36c014/14481/720p.mp4
- https://pod.univ-cotedazur.fr/media/videos/c15b2008a6a431b3c62a2c0b4f4a5f04548aa0f5a77088c653b52b400d36c014/14482/720p.mp4

### Cours 6 (Astrométrie)
- https://pod.univ-cotedazur.fr/media/videos/709b113f2cd8347d4a7c80f6fa8851de9cd4e63a6582a850a9b7b908cb35e981/15177/720p.mp4

### Cours 7 (Optique Atmosphérique)

Pré-cours:
- https://www.youtube.com/watch?v=Pr98VKZyOMQ

Cours:
- https://unice-my.sharepoint.com/:v:/g/personal/aziz_ziad_unice_fr/EVnNb6FJQlROg94hfLzqylgBYyrmOWAYBOl1l5f259nMWQ

### Cours 8 (Statistical Detection)

- https://pod.univ-cotedazur.fr/media/videos/709b113f2cd8347d4a7c80f6fa8851de9cd4e63a6582a850a9b7b908cb35e981/15600/720p.mp4

### Cours 9 (Spectroscopie)

Pré-cours:
- https://pod.univ-cotedazur.fr/media/videos/461b42c8975a968d17e1304b224311213bf072e33d7384dca69e805cf666a679/15567/720p.mp4
- https://pod.univ-cotedazur.fr/media/videos/461b42c8975a968d17e1304b224311213bf072e33d7384dca69e805cf666a679/15591/720p.mp4
- https://pod.univ-cotedazur.fr/media/videos/461b42c8975a968d17e1304b224311213bf072e33d7384dca69e805cf666a679/15593/720p.mp4
- https://pod.univ-cotedazur.fr/media/videos/461b42c8975a968d17e1304b224311213bf072e33d7384dca69e805cf666a679/15595/720p.mp4
- https://pod.univ-cotedazur.fr/media/videos/461b42c8975a968d17e1304b224311213bf072e33d7384dca69e805cf666a679/15596/720p.mp4
- https://pod.univ-cotedazur.fr/media/videos/461b42c8975a968d17e1304b224311213bf072e33d7384dca69e805cf666a679/15597/720p.mp4
- https://pod.univ-cotedazur.fr/media/videos/461b42c8975a968d17e1304b224311213bf072e33d7384dca69e805cf666a679/15599/720p.mp4
- https://youtu.be/psaQ9e01Ux8
- https://youtu.be/WLnz4wJ4DiU
- https://youtu.be/MWMrZJh5Et0

Cours:
- https://pod.univ-cotedazur.fr/media/videos/461b42c8975a968d17e1304b224311213bf072e33d7384dca69e805cf666a679/15687/720p.mp4

### Cours 10 (Spectrographes)

- https://pod.univ-cotedazur.fr/media/videos/709b113f2cd8347d4a7c80f6fa8851de9cd4e63a6582a850a9b7b908cb35e981/15675/720p.mp4

BdD spectres ESO :
- https://www.eso.org/sci/observing/tools/standards/spectra/hamuystandards.html

### Cours 11 (Télescopes)

- https://pod.univ-cotedazur.fr/media/videos/709b113f2cd8347d4a7c80f6fa8851de9cd4e63a6582a850a9b7b908cb35e981/15775/720p.mp4

### Cours 12 (Détecteurs)

- https://pod.univ-cotedazur.fr/media/videos/709b113f2cd8347d4a7c80f6fa8851de9cd4e63a6582a850a9b7b908cb35e981/15803/720p.mp4

### Cours 13 (Photométrie)

Pré-cours:
- https://pod.univ-cotedazur.fr/media/videos/709b113f2cd8347d4a7c80f6fa8851de9cd4e63a6582a850a9b7b908cb35e981/15848/720p.mp4
- https://pod.univ-cotedazur.fr/media/videos/709b113f2cd8347d4a7c80f6fa8851de9cd4e63a6582a850a9b7b908cb35e981/15844/720p.mp4
- https://pod.univ-cotedazur.fr/media/videos/709b113f2cd8347d4a7c80f6fa8851de9cd4e63a6582a850a9b7b908cb35e981/15842/720p.mp4

English version:
- https://pod.univ-cotedazur.fr/media/videos/709b113f2cd8347d4a7c80f6fa8851de9cd4e63a6582a850a9b7b908cb35e981/14439/720p.mp4
- https://pod.univ-cotedazur.fr/media/videos/709b113f2cd8347d4a7c80f6fa8851de9cd4e63a6582a850a9b7b908cb35e981/14438/720p.mp4

### Cours 14 (Photométrie AIJ)

Pré-cours:
- https://pod.univ-cotedazur.fr/media/videos/709b113f2cd8347d4a7c80f6fa8851de9cd4e63a6582a850a9b7b908cb35e981/14440/720p.mp4
- https://pod.univ-cotedazur.fr/media/videos/709b113f2cd8347d4a7c80f6fa8851de9cd4e63a6582a850a9b7b908cb35e981/14441/720p.mp4
- https://pod.univ-cotedazur.fr/media/videos/709b113f2cd8347d4a7c80f6fa8851de9cd4e63a6582a850a9b7b908cb35e981/14442/720p.mp4
- https://pod.univ-cotedazur.fr/media/videos/709b113f2cd8347d4a7c80f6fa8851de9cd4e63a6582a850a9b7b908cb35e981/14443/720p.mp4
- https://pod.univ-cotedazur.fr/media/videos/709b113f2cd8347d4a7c80f6fa8851de9cd4e63a6582a850a9b7b908cb35e981/14444/720p.mp4
- https://pod.univ-cotedazur.fr/media/videos/709b113f2cd8347d4a7c80f6fa8851de9cd4e63a6582a850a9b7b908cb35e981/14445/720p.mp4
- https://pod.univ-cotedazur.fr/media/videos/709b113f2cd8347d4a7c80f6fa8851de9cd4e63a6582a850a9b7b908cb35e981/14446/720p.mp4
- https://pod.univ-cotedazur.fr/media/videos/709b113f2cd8347d4a7c80f6fa8851de9cd4e63a6582a850a9b7b908cb35e981/14447/720p.mp4

Cours:
- https://pod.univ-cotedazur.fr/media/videos/709b113f2cd8347d4a7c80f6fa8851de9cd4e63a6582a850a9b7b908cb35e981/15883/720p.mp4
- https://pod.univ-cotedazur.fr/media/videos/709b113f2cd8347d4a7c80f6fa8851de9cd4e63a6582a850a9b7b908cb35e981/15882/720p.mp4
- https://pod.univ-cotedazur.fr/media/videos/709b113f2cd8347d4a7c80f6fa8851de9cd4e63a6582a850a9b7b908cb35e981/15886/720p.mp4
- https://pod.univ-cotedazur.fr/media/videos/709b113f2cd8347d4a7c80f6fa8851de9cd4e63a6582a850a9b7b908cb35e981/15887/720p.mp4
- https://pod.univ-cotedazur.fr/media/videos/709b113f2cd8347d4a7c80f6fa8851de9cd4e63a6582a850a9b7b908cb35e981/15888/720p.mp4

Useful:
- https://nova.astrometry.net/

### Cours 15 (LaTeX)

Cours:
- https://pod.univ-cotedazur.fr/media/videos/709b113f2cd8347d4a7c80f6fa8851de9cd4e63a6582a850a9b7b908cb35e981/15901/720p.mp4

BdD bibliographique en Astronomie:
- https://ui.adsabs.harvard.edu/

### Cours 16 (Python)

Pré-cours:
- Moodle Python : https://www.youtube.com/channel/UCIlUBOXnXjxdjmL_atU53kA/videos
- Cours Python : https://courspython.com/sommaire.html
